package com.sbt.school.l11.part2.account;

import com.sbt.school.tools.Tools;

import static java.lang.String.format;

/**
 * data race
 * race condition
 */
public class FailTransferMachine implements TransferMachine {

    public void transfer(int amount, Account from, Account to) {
        if (from.getBalance() > amount) {
            from.setBalance(from.getBalance() - amount);
            Tools.cpuBurn(100_000);
            to.setBalance(to.getBalance() + amount);
        }
        print(amount, from, to);
    }
}

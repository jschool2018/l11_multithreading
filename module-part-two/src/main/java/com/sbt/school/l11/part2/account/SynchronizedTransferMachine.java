package com.sbt.school.l11.part2.account;

import com.sbt.school.tools.Tools;

import static java.lang.String.format;

/**
 * no data race
 * no race condition
 */
public class SynchronizedTransferMachine implements TransferMachine {

    public void transfer(int amount, Account from, Account to) {
        synchronized (this) {
            if (from.getBalance() > amount) {
                from.setBalance(from.getBalance() - amount);
                Tools.cpuBurn(100_000);
                to.setBalance(to.getBalance() + amount);
            }
            print(amount, from, to);
        }
        Thread.yield(); // to get some fairness
    }
}

package com.sbt.school.l11.part2.account;

import static java.lang.String.format;

public interface TransferMachine {
    void transfer(int amount, Account from, Account to);

    default void print(int amount, Account from, Account to) {
        System.out.println(
                format("%s - transfer %10d, from %20d, to %10d, sum %10d",
                        Thread.currentThread().getName(),
                        amount,
                        from.getBalance(),
                        to.getBalance(),
                        from.getBalance() + to.getBalance()));
    }
}

package com.sbt.school.l11.jmh;

import org.openjdk.jmh.annotations.Benchmark;
import org.openjdk.jmh.annotations.Scope;
import org.openjdk.jmh.annotations.State;
import org.openjdk.jmh.runner.Runner;
import org.openjdk.jmh.runner.RunnerException;
import org.openjdk.jmh.runner.options.Options;
import org.openjdk.jmh.runner.options.OptionsBuilder;

@State(Scope.Thread)
public class PipelineTest {

    double d1 = 1_000_000_000F;
    double d2 = 2_000_000_000F;
    double d3 = 3_000_000_000F;
    double d4 = 4_000_000_000F;

    double d5 = 8_000_000_000F;
    double d6 = 9_000_000_000F;
    double d7 = 10_000_000_000F;
    double d8 = 11_000_000_000F;

    double d11 = 1_000_000_000F;
    double d12 = 2_000_000_000F;
    double d13 = 3_000_000_000F;
    double d14 = 4_000_000_000F;

    double d15 = 8_000_000_000F;
    double d16 = 9_000_000_000F;
    double d17 = 10_000_000_000F;
    double d18 = 11_000_000_000F;


    @Benchmark
    //@OperationsPerInvocation(1_000_000)
    public double loop1() {
        for (int i = 0; i < 1_000_000 ; i++) {
            d1 = d1 * d1;
        }
        d1 = d1 + d1 + d1 + d1 + d1 + d1 + d1 + d1;
        return d1;
    }

    @Benchmark
    //@OperationsPerInvocation(1_000_000)
    public double loop2() {
        for (int i = 0; i < 1_000_000 ; i++) {
            d1 = d1 * d1;
            d2 = d2 * d2;
            d3 = d3 * d3;
            d4 = d4 * d4;
        }
        d1 = d1 + d2 + d3 + d4 + d1 + d2 + d3 + d4;
        return d1;
    }

    @Benchmark
    //@OperationsPerInvocation(1_000_000)
    public double loop3() {
        for (int i = 0; i < 1_000_000 ; i++) {
            d1 = d1 * d1;
            d2 = d2 * d2;
            d3 = d3 * d3;
            d4 = d4 * d4;
            d5 = d5 * d5;
            d6 = d6 * d6;
            d7 = d7 * d7;
            d8 = d8 * d8;
        }
        d1 = d1 + d2 + d3 + d4 + d5 + d6 + d7 + d8;
        return d1;
    }

    @Benchmark
    //@OperationsPerInvocation(1_000_000)
    public double loop4() {
        for (int i = 0; i < 1_000_000 ; i++) {
            d1 = d1 * d1;
            d2 = d2 * d2;
            d3 = d3 * d3;
            d4 = d4 * d4;
            d5 = d5 * d5;
            d6 = d6 * d6;
            d7 = d7 * d7;
            d8 = d8 * d8;

            d11 = d11 * d11;
            d12 = d12 * d12;
            d13 = d13 * d13;
            d14 = d14 * d14;
            d15 = d15 * d15;
            d16 = d16 * d16;
            d17 = d17 * d17;
            d18 = d18 * d18;
        }
        d1 = d1 + d2 + d3 + d4 + d5 + d6 + d7 + d8 +
             d11 + d12 + d13 + d14 + d15 + d16 + d17 + d18;
        return d1;
    }


    public static void main(String[] args) throws RunnerException {
        Options opt = new OptionsBuilder()
                .include(PipelineTest.class.getSimpleName())
                .warmupIterations(5)
                .measurementIterations(5)
                .forks(1)
                .build();

        new Runner(opt).run();
    }
}

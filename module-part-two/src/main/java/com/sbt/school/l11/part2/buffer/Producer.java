package com.sbt.school.l11.part2.buffer;

public class Producer implements Runnable {
    private int startValue;
    private int period;
    private SingleElemenetBuffer buffer;

    public Producer(int startValue, int period, SingleElemenetBuffer buffer) {
        this.startValue = startValue;
        this.period = period;
        this.buffer = buffer;
    }

    @Override
    public void run() {
        while (true) {
            try {
                System.out.println(startValue + " produced");
                buffer.put(startValue++);
                Thread.sleep(period);
            } catch (InterruptedException e) {
                System.out.println(Thread.currentThread().getName() + " stopped");
                break;
            }
        }
    }
}

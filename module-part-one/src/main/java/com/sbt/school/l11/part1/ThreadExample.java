package com.sbt.school.l11.part1;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * run start test
 */
public class ThreadExample {
    static private Logger logger = LoggerFactory.getLogger(ThreadExample.class);


    public static void main(String[] args) {

        Runnable test = new Runnable() {
            @Override
            public void run() {
                logger.info("test {}", Thread.currentThread().getName());
                System.out.println();
            }
        };

        test.run();

        new Thread(test).start();

    }
}

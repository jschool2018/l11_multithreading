package com.sbt.school.l11.part1;

public class RunnableExample {
    public static void main(String[] args) {
        Runnable test = () -> System.out.println("test");
        System.out.println(test.getClass().getName());
    }
}

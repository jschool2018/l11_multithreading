package com.sbt.school.l11.part1;

import static java.lang.String.format;

public class PriorityExample implements Runnable{
    @Override
    public void run() {
        for (int i = 0; i < 3; i++) {
            System.out.println(format("#%s(%d)",
                    Thread.currentThread().getName(),
                    Thread.currentThread().getPriority()
            ));
        }
    }

    public static void main(String[] args) {
        for (int i = 0; i < 4; i++) {
            Thread thread = new Thread(new PriorityExample());
            thread.setPriority(i % 2 == 0 ? Thread.MAX_PRIORITY : Thread.MIN_PRIORITY);
            thread.start();
        }
    }
}

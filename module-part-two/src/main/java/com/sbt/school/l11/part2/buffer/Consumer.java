package com.sbt.school.l11.part2.buffer;

public class Consumer implements Runnable {
    private SingleElemenetBuffer buffer;

    public Consumer(SingleElemenetBuffer buffer) {
        this.buffer = buffer;
    }

    @Override
    public void run() {
        while (true) {
            try {
                int result = buffer.get();
                System.out.println(result + " got");
            } catch (InterruptedException e) {
                System.out.println(Thread.currentThread().getName() + " stopped");
                break;
            }
        }
    }
}

package com.sbt.school.l11.part1;

import java.util.concurrent.TimeUnit;


/**
 * jps
 * jstack
 * stat test
 */
public class ThreadState {
    public static void main(String[] args) throws InterruptedException {
        Object lock = new Object();

        Thread threadFiniteWait0 = new Thread(() -> {
            synchronized (lock) {
                try {
                    lock.wait(300_000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }, "th_finiteWait0");
        threadFiniteWait0.start();

        Thread.sleep(1000);

        Thread threadRunning = new Thread(() -> {
            synchronized (lock) {
                while (true);
            }
        }, "th_threadRunning");
        threadRunning.start();

        Thread threadSleep = new Thread(() -> {
            try {
                TimeUnit.HOURS.sleep(10);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }, "th_sleep");
        threadSleep.start();

        Thread threadEndlessWait = new Thread(() -> {
            synchronized (lock) {
                try {
                    lock.wait();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }, "th_endlessWait");
        threadEndlessWait.start();

        Thread threadFiniteWait1 = new Thread(() -> {
            synchronized (lock) {
                try {
                    lock.wait(300_000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }, "th_finiteWait1");
        threadFiniteWait1.start();


//        Thread threadJoined = new Thread(() -> {
//        }, "th_threadJoined");
//        threadJoined.start();
//        threadJoined.join();
    }
}

package com.sbt.school.l11.part2.account;


/**
 *                   | data race                          | no data race
 * -----------------------------------------------------------------------------------
 * race condition    | FailTransferMachine         (fail)  | OnlyRaceConditionTransferMachine (fail)
 * no race condition | OnlyDataRaceTransferMachine         | SynchronizedTransferMachine
 */

public class LauncherTransferring {
    public static void main(String[] args) {
        Account from = new Account(800);
        Account to = new Account(200);
        //TransferMachine transferMachine = new FailTransferMachine();
        //TransferMachine transferMachine = new OnlyRaceConditionTransferMachine();
        TransferMachine transferMachine = new SynchronizedTransferMachine();
        //TransferMachine transferMachine = new OnlyDataRaceTransferMachine();

        for (int i = 0; i < 2; i++) {
            new Thread(() -> {
                while(true) {
                    transferMachine.transfer(40, from, to);
                }
            }, "from " + i).start();
        }


        for (int i = 0; i < 2; i++) {
            new Thread(() -> {
                while(true) {
                    transferMachine.transfer(30, to, from);
                }
            }, "to   " + i).start();
        }
    }
}

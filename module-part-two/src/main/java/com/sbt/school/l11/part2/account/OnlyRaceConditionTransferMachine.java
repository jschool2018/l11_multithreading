package com.sbt.school.l11.part2.account;

import com.sbt.school.tools.Tools;

import static java.lang.String.format;

/**
 * no data race
 * race condition
 */
public class OnlyRaceConditionTransferMachine implements TransferMachine {

    public void transfer(int amount, Account from, Account to) {
        int balance;
        synchronized (this) {
            balance = from.getBalance();
        }

        if (balance > amount) {
            synchronized (this) {
                from.setBalance(from.getBalance() - amount);
            }
            Tools.cpuBurn(100_000);
            synchronized (this) {
                to.setBalance(to.getBalance() + amount);
            }
        }
        print(amount, from, to);
    }
}

package com.sbt.school.l11.part2;

public class SimpleTest {

    public static void main(String[] args) throws InterruptedException {

        //new Object().wait(); //error

        //new Object().notify(); //error

        //new Object().notifyAll(); //error

        synchronized (new Object()) {
        }

//        synchronized (new Object()) { //error
//            new Object().wait();
//        }
    }
}


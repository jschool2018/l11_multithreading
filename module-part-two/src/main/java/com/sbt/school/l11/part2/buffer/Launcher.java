package com.sbt.school.l11.part2.buffer;

public class Launcher {
    public static void main(String[] args) {
        SingleElemenetBuffer buffer = new SingleElemenetBuffer();
        new Thread(new Producer(1, 1000, buffer)).start();
        new Thread(new Producer(100, 500, buffer)).start();
        new Thread(new Consumer(buffer)).start();

    }
}

package com.sbt.school.l11.part2;

import java.util.concurrent.TimeUnit;

public class ThreadStop {
    private static boolean flag;
    //private static volatile boolean flag;

    public static void main(String[] args) throws InterruptedException {
        new Thread(() -> {
            System.out.println("Thread start");
            int i = 0;
            while(!flag) {
                i++;
            }
            System.out.println("Thread stop");
        }).start();

        System.out.println("main start");
        TimeUnit.SECONDS.sleep(2);
        flag = true;
        System.out.println("main stop");
    }
}

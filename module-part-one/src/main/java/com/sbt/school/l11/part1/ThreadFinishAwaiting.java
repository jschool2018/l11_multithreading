package com.sbt.school.l11.part1;

import com.sbt.school.tools.Tools;

/**
 * join test
 */
public class ThreadFinishAwaiting {
    public static void main(String[] args) throws InterruptedException {
        Thread thread1 = new Thread(() -> {
            for (int i = 0; i < 10; i++) {
                System.out.println(Thread.currentThread().getName() + " " + i);
                Tools.cpuBurn();
            }
        });
        thread1.start();

        for (int i = 0; i < 3; i++) {
            System.out.println(Thread.currentThread().getName() + " " + i);
            Tools.cpuBurn();
        }

        thread1.join();
//        thread1.join(1000);

        System.out.println("the end");

    }
}

package com.sbt.school.tools;

public class Tools {
    public static double cpuBurn() {
        return cpuBurn(10_000L);
    }

    public static double cpuBurn(long count) {
        double result = 0;
        for (int i = 0; i < count; i++) {
            result = Math.sin(result + i);
        }
        return result;
    }

}
